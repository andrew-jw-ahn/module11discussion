import java.util.*;

public class Anchorage
{
    public static void main (String args [])
    {
        ArrayList<Ship> anchorageQueue = new ArrayList<Ship>();
        Ship stingray = new Ship("Stingray", 6, 11);
        Ship everGiven = new Ship("Ever Given", 6, 23);
        Ship partyBoat = new Ship("Party Boat", 10, 19);
        Ship orca = new Ship("Orca", 10, 12);
        anchorageQueue.add(stingray);
        anchorageQueue.add(everGiven);
        anchorageQueue.add(partyBoat);
        anchorageQueue.add(orca);
        Collections.sort(anchorageQueue,
            Comparator.comparing(Ship::getPriority, Comparator.reverseOrder())
            .thenComparing(Ship::getArrivalTime));
        System.out.println("\n");
        
        for (Ship vessel : anchorageQueue)
        {
            System.out.println(vessel.toString());
        }
    }
}