public class Ship
{
    private int priority;
    private int arrivalTime;
    private String shipName;
    Ship(String shipName, int priority, int arrivalTime)
    {
        this.shipName = shipName;
        this.priority = priority;
        this.arrivalTime = arrivalTime;
    }
    public int getPriority()
    {
        return priority;
    }
    public int getArrivalTime()
    {
        return arrivalTime;
    }
    public String getShipName()
    {
        return shipName;
    }
    public String toString()
    {
        return shipName + "\t\t" + Integer.toString(priority) + "\t"
            +"Hour Arrival:" + Integer.toString(arrivalTime);
    }
}